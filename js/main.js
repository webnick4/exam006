$(function() {

  var getMessageHtml = function(item) {
    var user = item.user;
    var list = $('#list');
    var bodyElement = $('<div class="massages">');
    var authorElement = $('<p>' + user.firstName + ' ' + user.lastName + ' сказал' + '</p>');
    var massageElement = $('<p>' + item.message + '</p>');
    bodyElement.append(authorElement, massageElement);
    list.append(bodyElement);
  };

  $('#send-massage').on('click', function() {
    var messageElement = $('#message').val();

    $.ajax({
      method: 'POST',
      url: 'http://146.185.154.90:8000/blog/myprofile@gmail.com/posts',
      data: {
        message: messageElement
      }
    });
  });

  $.getJSON('http://146.185.154.90:8000/blog/myprofile@gmail.com/posts').then(function(response) {
    console.log(response);
    var responseData = response[0];
    var user = responseData.user;
    var myName = $('#myName');
    var getNameElement = $('<span>' + user.firstName + ' ' + user.lastName + '</span>');
    myName.append(getNameElement);

    response.forEach(getMessageHtml);


    var getDateTime = response[response.length - 1].datetime;
    setInterval(function() {
      $.getJSON('http://146.185.154.90:8000/blog/myprofile@gmail.com/posts?datetime=' + getDateTime).then(function(response) {
        response.forEach(getMessageHtml);
      });
    }, 3000);

  });


  $('#btn-add-email').on('click', function() {
    var email = $('#inputEmail').val();

    $.ajax({
      method: 'POST',
      url: 'http://146.185.154.90:8000/blog/myprofile@gmail.com/subscribe',
      data: {
        email: email
      }
     });
  });

  $('#btn-save-profile').on('click', function() {
    var firstName = $('#first-name').val();
    var lastName = $('#last-name').val();
    $.ajax({
      method: 'POST',
      url: 'http://146.185.154.90:8000/blog/myprofile@gmail.com/profile',
      data: {
        firstName: firstName,
        lastName: lastName
      }
    });
  });

  $('#edit-profile').on('click', function() {
    var body = '<form class="form-horizontal">\n' +
      '      <div class="form-group">\n' +
      '      <label for="first-name" class="col-sm-2 control-label">First name</label>\n' +
      '    <div class="col-sm-10">\n' +
      '      <input type="text" class="form-control" id="first-name" placeholder="First Name">\n' +
      '      </div>\n' +
      '      </div>\n' +
      '      <div class="form-group">\n' +
      '      <label for="last-name" class="col-sm-2 control-label">Last name</label>\n' +
      '    <div class="col-sm-10">\n' +
      '      <input type="text" class="form-control" id="last-name" placeholder="Last name">\n' +
      '      </div>\n' +
      '      </div>\n' +
      '      <div class="form-group">\n' +
      '      <div class="col-sm-offset-2 col-sm-10">\n' +
      '      <button type="submit" class="btn btn-default" id="btn-save-profile">Save</button>\n' +
      '      </div>\n' +
      '      </div>\n' +
      '      </form>';


    editProfile({
      header: 'Edit profile',
      body: body,
      callback: function() {
        console.log('OK button on clicked');
      }
    });
  });

  function editProfile(params) {
    $('#dialog').fadeIn(1000);
    $('#header').html(params.header);
    $('#body').html(params.body);
    $('#btn-save-profile').on('click', function () {
      if (params.callback) params.callback();
      $('#dialog').fadeOut(1000);
    });
  }

  $('#add-email').on('click', function() {
    var body = '<form class="form-horizontal">\n' +
      '    <div class="form-group">\n' +
      '      <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>\n' +
      '      <div class="col-sm-10">\n' +
      '        <input type="email" class="form-control" id="inputEmail" placeholder="Email">\n' +
      '      </div>\n' +
      '    </div>\n' +
      '    <div class="form-group">\n' +
      '      <div class="col-sm-offset-2 col-sm-10">\n' +
      '        <button type="submit" class="btn btn-default" id="btn-add-email">Add</button>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '  </form>';

    addEmail({
      header: 'Follow user',
      body: body,
      callback: function() {
        console.log('OK button on clicked');
      }
    });
  });

  function addEmail(params) {
    $('#eddEmailDialog').fadeIn(1000);
    $('#eddEmailHeader').html(params.header);
    $('#eddEmailBody').html(params.body);
    $('#btn-add-email').on('click', function () {
      if (params.callback) params.callback();
      $('#eddEmailDialog').fadeOut(1000);
    });
  }

});